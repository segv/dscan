<!DOCTYPE html>
<html>
    <head>
        <title>PL Dscan Analyzer</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <style>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        </style>
        <script src=https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js></script>
        <script language="JavaScript">
        $(document).ready(function() {
            $('button[role="group-by"]').click(function (e) {
                e.preventDefault();
                $('[role="tabpanel"]').toggleClass('active');
                $(e.toElement).toggleClass('btn-primary');
            })
        });
        </script>
    </head>
    <body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">PL Dscan Analyzer</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/new">New</a></li>
                </ul>
            </div>
    <!-- Navbar Contents -->
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $system }} at {{ $time }} UTC on {{ $date }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button role="group-by" type="button" class="btn btn-primary pull-right">
                    group
                </button>
            </div>
        </div>
        <div class="row">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                        All ships &nbsp;&nbsp;<span class="badge">{{$total}}</span></a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane" id="ungrouped">
                    @foreach ($ungrouped as $title => $type)
                    <div class="col-md-4">
                        <h3>
                            {{ $title }}
                            <span class="badge">{{ $sizes[$title] }}</span>
                        </h3>
                        <ul class="list-group">
                            @foreach ($type as $name => $entries)
                                <li class="list-group-item">
                                    <span class="badge">{{ count($entries) }}</span>
                                    {{ $name }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @endforeach
                </div>
                <div role="tabpanel" class="tab-pane active" id="grouped">
                    @foreach ($grouped as $title => $group)
                    <div class="col-md-4">
                        <h3>
                            {{ $title }}
                            <span class="badge">{{ $sizes[$title] }}</span>
                        </h3>
                        <ul class="list-group">
                            @foreach ($group as $groupname => $capitals)
                                <li class="list-group-item active">
                                    <?php 
                                        $sum = array_sum(
                                                array_map(
                                                    function($e) {
                                                        return count($e);
                                                    }, $capitals));
                                    ?>
                                    <span class="badge">{{ $sum }}</span>
                                    {{ $groupname }}
                                </li>
                                @foreach ($capitals as $name => $entries)
                                <li class="list-group-item">
                                    <span class="badge">{{ count($entries) }}</span>
                                    {{ $name }}
                                </li>
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>	
    </div>
    </body>
</html>
