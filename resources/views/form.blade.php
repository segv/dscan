<!DOCTYPE html>
<html>
    <head>
        <title>PL Dscan Analyzer</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <style>
        .bs {
            background-color: rgb(255, 255, 255);
            border-color: rgb(221, 221, 221);
            border-style: solid;
            border-width: 1px;
            padding: 15px;
        }
        .bs label {
            color: rgb(100, 100, 100);
        }
        </style>
    </head>
    <body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">PL Dscan Analyzer</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/new">New</a></li>
                </ul>
            </div>
    <!-- Navbar Contents -->
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="bs" action="/analyze" method="POST">
                    <div class="form-group">
                        <label>Directional Scan</label>
                        <textarea name="dscan" class="form-control" rows="8"></textarea>
                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-default">Analyze</button>
                </form>
            </div>
        </div>	
    </div>
    </body>
</html>
