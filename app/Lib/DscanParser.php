<?php

namespace App\Lib;

use NumberFormatter;
use DB;

class DscanResult
{
    public $system = null;
    public $items = null;
    public $subcapitals = null;
    public $capitals = null;
    public $structures = null;

    function __construct() {
        $this->items = collect([]);
    }
}

class DscanParser
{
    const AU = 1.496e8;

    private $categories = [
        'Ship',
        'Structure',
        'Starbase',
        'Deployable',
    ];

    private $capitals = [
        'carrier',
        'supercarrier',
        'titan',
        'capital industrial ship',
        'dreadnought',
    ];

    private $structures = [
        'starbase',
        'structure',
        'deployable',
    ];

    function __construct()
    {
    }

    public function hash($dscan)
    {
        $lines = explode("\n", $dscan);
        sort($lines);
        return sha1(implode("\n", $lines));
    }

    public function parse($dscan)
    {
        $lines = explode("\n", $dscan);

        $parsed = collect($lines)
            ->map(
                function($k) {
                    return self::parseLine($k);
                })
            ->reject('is_null');
        $groups = $parsed->groupBy('type');
        $types = $groups->keys();

        if ($types->isEmpty()) {
            return $result;
        }

        $categories = $this->categorize($types);

        $result = new DscanResult();
        foreach($categories as $item) {
            foreach($groups[$item->typeName] as $element) {
                $result->items->push(
                    ['name' => $element['name'],
                     'type' => $item->typeName,
                     'typeId' => $item->typeID,
                     'group' => $item->groupName,
                     'category' => $item->categoryName,
                     'distance' => $element['distance']]);
            }
        }


        $isShip = function($v) {
            return strtolower($v['category']) == 'ship';
        };

        $isCapital = function($v) {
            return collect($this->capitals)
                ->contains(strtolower($v['group']));
        };

        $isStructure = function($v) {
            return collect($this->structures)
                ->contains(strtolower($v['category']));
        };

        $ships = $result->items->filter($isShip); 
        $result->capitals = $ships->filter($isCapital);
        $result->subcapitals = $ships->reject($isCapital);
        $result->structures = $result->items->filter($isStructure);
        $result->system = self::getSolarSystem($groups);

        return $result;
    }

    private function categorize($types) {
        $results = DB::connection('neweden')
                    ->table('invTypes')
                    ->select('invTypes.typeID', 'invTypes.typeName',
                             'invGroups.groupName', 'invCategories.categoryName')
                    ->join('invGroups', 'invTypes.groupID', '=', 'invGroups.groupID')
                    ->join('invCategories', 'invCategories.categoryID', '=', 'invGroups.categoryID')
                    ->whereIn('invTypes.typeName', $types->toArray())
                    ->whereIn('invCategories.categoryName', $this->categories)
                    ->groupBy('invTypes.typeID')
                    ->get();
        return collect($results);
    }

    private static function getSolarSystem($groups) {
        $system = $groups->keys()->first(function($k, $v) {
            return strpos($v, 'Sun') === 0;
        });

        if (!isset($groups[$system])) {
            return null;
        }

        $entry = $groups[$system]->first();
        if (preg_match('@([\w\-]+) \- Star$@', $entry['name'], $m)) {
            return $m[1]; 
        }
        return null;
    }

    private static function parseLine($line) {
        $line = trim($line);
        $tuple = explode("\t", $line);
        if (count($tuple) < 3) {
            return null;
        }

        list($name, $type, $distance) = $tuple;
        $dist = self::convertDistance($distance);

        if (!isset($groups[$type])) {
            $groups[$type] = [];
        }

        return ['name' => $name, 'type' => $type, 'distance' => $dist];
    }

    private static function convertDistance($dist) {
        if (preg_match('@^([\d\.,]+) (m|km|AU)$@', $dist, $m)) {
            $fmt = new NumberFormatter('en_US', NumberFormatter::DECIMAL);
            list($_, $num, $unit) = $m;
            $km = $fmt->parse($num);

            if ($unit == 'AU') {
                return $km * self::AU;
            }

            if ($unit == 'm') {
                return $km / 1000.0;
            }

            return $km;
        }

        if ($dist == '-') {
            return null;
        }

        throw new \UnexpectedValueException();
    }
}
