<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dscan extends Model
{
    protected $table = 'scans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = ['dscan', 'hash'];
}
