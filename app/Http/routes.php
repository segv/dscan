<?php

use Illuminate\Http\Request;
use App\Lib\DscanParser;
use App\Dscan;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('form');
});

Route::get('/new', function () {
    return view('form');
});

Route::post('/analyze', function(Request $req) {
    $input = $req->input('dscan');

    $dp = new DscanParser();
    $hash = $dp->hash($input);
    $exists = Dscan::where('hash', $hash)->first();
    if ($exists == null) {
        $dscan = new Dscan;
        $dscan->dscan = $input;
        $dscan->hash = $hash;
        $dscan->save();
    }

    return redirect('/analyze/'.$hash);
});

Route::get('/info', function () {
    phpinfo();
});

Route::get('/analyze/{id}', function ($id) {
    $dscan = Dscan::where('hash', $id)->first();
    if ($dscan == null) {
        App::abort(404);
    }

    $dp = new DscanParser();
    $result = $dp->parse($dscan->dscan);

    $sorting = function($xs) {
        return count($xs);
    };

    $grouping = function($xs) use ($sorting) {
        if (empty($xs)) {
            return $xs;
        }
        return $xs->groupBy('type')
            ->sortByDesc($sorting);
    };

    $arr = [
        'Capitals' => $result->capitals,
        'Subcapitals' => $result->subcapitals,
        'Structures' => $result->structures,
    ];

    $ungrouped = collect($arr)->map(
        function($r) use ($sorting) {
            return $r->groupBy('type')
                   ->sortByDesc($sorting);
            return $r;
        }
    );

    $grouped = collect($arr)->map(
        function($r) use ($grouping) {
            $g = $r->groupBy('group')
                   ->map($grouping)
                   ->toArray();
            arsort($g);
            reset($g);
            return $g;
        }
    );

    $sizes = collect($arr)->map(
        function($r) {
            return count($r);
        }
    );

    $created_at = DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $dscan->created_at,
                    new DateTimeZone("UTC"));

    return view('dscan', [
        'system' => $result->system ?: 'Unknown',
        'time' => $created_at->format('H:i:s'),
        'date' => $created_at->format('Y-m-d'),
        'ungrouped' => $ungrouped,
        'grouped' => $grouped,
        'sizes' => $sizes,
        'total' => $sizes->sum(),
    ]);
});

function sortOrderCapitals($v) {
    switch ($v) {
        case 'titan':
            return $v;
        case 'supercarrier':
            return $v * 100;
        default:
            return $v * 1000;
    }
}
